CREATE TABLE conta (
    id SERIAL PRIMARY KEY,
    data_vencimento DATE,
    data_pagamento DATE,
    descricao VARCHAR(255),
    situacao VARCHAR(255),
    valor DOUBLE PRECISION
   );
