/**
 * View Models used by Spring MVC REST controllers.
 */
package com.renango.pagador.web.rest.vm;
