package com.renango.pagador.service;

import com.renango.pagador.domain.Conta;

import java.util.List;

public interface CSVService {

    List<Conta> getContasFromFile(String file);
}
