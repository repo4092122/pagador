package com.renango.pagador.service.mapper;

import com.renango.pagador.domain.Conta;
import com.renango.pagador.service.dto.ContaDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Conta} and its DTO {@link ContaDTO}.
 */
@Mapper(componentModel = "spring")
public interface ContaMapper extends EntityMapper<ContaDTO, Conta> {}
