package com.renango.pagador.service;


import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private Map<String, UserDetails> users = new HashMap<>();

    @PostConstruct
    public void init() throws IOException {
        ClassPathResource resource = new ClassPathResource("users.txt");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("=");
                String username = parts[0];
                getUsername result = new getUsername(parts, username);
                String[] details = result.parts[1].split(",");
                String password = details[0];
                List<SimpleGrantedAuthority> authorities = List.of(details[1].split(",")).stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
                users.put(result.username, new User(result.username, password, authorities));
            }
        }
    }

    private static class getUsername {
        public final String[] parts;
        public final String username;

        public getUsername(String[] parts, String username) {
            this.parts = parts;
            this.username = username;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = users.get(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }
}
