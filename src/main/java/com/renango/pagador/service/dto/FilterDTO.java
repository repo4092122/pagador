package com.renango.pagador.service.dto;

import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;


public class FilterDTO {
    private final LocalDate dataVencimento;
    private final String descricao;
    private final Pageable pageable;

    public FilterDTO(@RequestParam(required = false) LocalDate dataVencimento, @RequestParam(required = false) String descricao, Pageable pageable) {
        this.dataVencimento = dataVencimento;
        this.descricao = descricao;
        this.pageable = pageable;
    }

    public LocalDate getDataVencimento() {
        return dataVencimento;
    }

    public String getDescricao() {
        return descricao;
    }

    public Pageable getPageable() {
        return pageable;
    }
}
