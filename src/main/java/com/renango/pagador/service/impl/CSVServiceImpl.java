package com.renango.pagador.service.impl;

import com.renango.pagador.domain.Conta;
import com.renango.pagador.service.CSVService;
import com.renango.pagador.web.rest.errors.BadRequestAlertException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CSVServiceImpl implements CSVService {

    private static int DATA_VENCIMENTO = 0;
    private static int DATA_PAGAMENTO = 1;
    private static int DESCRICAO = 2;
    private static int SITUACAO = 3;
    private static int VALOR = 4;

    @Override
    public List<Conta> getContasFromFile(String csvContent) {
        try (StringReader stringReader = new StringReader(csvContent);
             CSVParser csvParser = new CSVParser(stringReader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {
            List<Conta> contas = new ArrayList<>();
            csvParser.getRecords().forEach(
                csvRecord -> contas.add(contaBuilder(csvRecord))
            );
            return contas;
        } catch (IOException e) {
            throw new BadRequestAlertException("Erro ao processar arquivo CSV: " + e.getMessage(), "CSVService", "csvError");
        }
    }

    private static Conta contaBuilder(CSVRecord csvRecord) {
        Conta conta = new Conta();
        conta.setDataVencimento(getDateFromString(csvRecord.get(DATA_VENCIMENTO)));
        conta.setDataPagamento(getDateFromString(csvRecord.get(DATA_PAGAMENTO)));
        conta.setDescricao(csvRecord.get(DESCRICAO));
        conta.setSituacao(csvRecord.get(SITUACAO));
        conta.setValor(Double.parseDouble(csvRecord.get(VALOR)));
        return conta;
    }

    private static LocalDate getDateFromString(String date){
        return Optional.ofNullable(date)
            .map(strDate-> strDate.isBlank() ? null : LocalDate.parse(strDate))
            .orElse(null);
    }
}
