package com.renango.pagador.service.impl;

import com.renango.pagador.domain.Conta;
import com.renango.pagador.repository.ContaRepository;
import com.renango.pagador.service.CSVService;
import com.renango.pagador.service.ContaService;
import com.renango.pagador.service.dto.ContaDTO;
import com.renango.pagador.service.dto.FilterDTO;
import com.renango.pagador.service.mapper.ContaMapper;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

import com.renango.pagador.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Conta}.
 */
@Service
@Transactional
public class ContaServiceImpl implements ContaService {

    private final Logger log = LoggerFactory.getLogger(ContaServiceImpl.class);

    private final ContaRepository contaRepository;

    private final ContaMapper contaMapper;

    private final CSVService csvService;
    private static final String ENTITY_NAME = "ContaService";

    public ContaServiceImpl(ContaRepository contaRepository, ContaMapper contaMapper, CSVService csvService) {
        this.contaRepository = contaRepository;
        this.contaMapper = contaMapper;
        this.csvService = csvService;
    }

    @Override
    public ContaDTO save(ContaDTO contaDTO) {
        log.debug("Request to save Conta : {}", contaDTO);
        Conta conta = contaMapper.toEntity(contaDTO);
        conta = contaRepository.save(conta);
        return contaMapper.toDto(conta);
    }

    public void loadContasByCSV(String content){
        csvService.getContasFromFile(content)
            .forEach(contaRepository::save);
    }

    @Override
    public ContaDTO update(ContaDTO contaDTO) {
        log.debug("Request to update Conta : {}", contaDTO);
        Conta conta = contaMapper.toEntity(contaDTO);
        conta.setIsPersisted();
        conta = contaRepository.save(conta);
        return contaMapper.toDto(conta);
    }

    @Override
    public Optional<ContaDTO> partialUpdate(ContaDTO contaDTO) {
        log.debug("Request to partially update Conta : {}", contaDTO);

        return contaRepository
            .findById(contaDTO.getId())
            .map(existingConta -> {
                contaMapper.partialUpdate(existingConta, contaDTO);

                return existingConta;
            })
            .map(contaRepository::save)
            .map(contaMapper::toDto);
    }

    @Override
    public ContaDTO updateSituacao(Long id, String situacao){
        return contaRepository.findById(id)
            .map(conta -> {
                conta.setSituacao(situacao);
                Conta contaAtualizada = contaRepository.save(conta);
                return contaMapper.toDto(contaAtualizada);
            }).orElseThrow( () -> new BadRequestAlertException("Conta não encontrada", ENTITY_NAME, "contaNotFound"));
    }


    @Override
    public Page<Conta> findByFilters(LocalDate dataVencimento, String descricao, Pageable pageable){
        return contaRepository.findByFilters(dataVencimento, descricao, pageable);
    }

    @Override
    public Page<Double> getTotalPagoByPeriodo(LocalDate dataInicio, LocalDate dataFim, Pageable pageable){
        return contaRepository.findTotalPagoByPeriodo(dataInicio, dataFim, pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<ContaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Contas");
        return contaRepository.findAll(pageable).map(contaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ContaDTO> findOne(Long id) {
        log.debug("Request to get Conta : {}", id);
        return contaRepository.findById(id).map(contaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Conta : {}", id);
        contaRepository.deleteById(id);
    }
}
