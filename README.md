# PAGADOR



## Getting started

To run as a develop, you should choose a profile to run into.
I'm used to run as DEV profile, so you can put the statement bellow on you vm options

```
-Dspring.profiles.active=dev
```
It's important to say: POSTGRESQL is a database required to run the application

## Dockering 

To run the project into a docker container, just run

```
docker-compose build
```

And, after that

```
docker-compose up
```

## Authentication 
To authenticate into this application, you should use

```
curl --location 'localhost:8081/authenticate' \
--header 'Content-Type: application/json' \
--data '{
    "username":"user",
    "password":"userPassword"
}'
```

Done that, it should retrieve a new token, and you need to use it as a BEARER. e.g :

```
curl --location 'localhost:8081/api/contas/' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNzE3MTE2MjkwLCJpYXQiOjE3MTcwODAyOTB9.l1cYtM5bR0eWOPlXa_FyO4P9dp17_-Z72pnLnmlcDag' \
'
```

There is a CSV file to load some data to system and use it, it is located at
```
/resources/contas.csv
```
