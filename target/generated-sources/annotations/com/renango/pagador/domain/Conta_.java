package com.renango.pagador.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Conta.class)
public abstract class Conta_ {

	public static volatile SingularAttribute<Conta, String> situacao;
	public static volatile SingularAttribute<Conta, LocalDate> dataPagamento;
	public static volatile SingularAttribute<Conta, LocalDate> dataVencimento;
	public static volatile SingularAttribute<Conta, Double> valor;
	public static volatile SingularAttribute<Conta, Long> id;
	public static volatile SingularAttribute<Conta, String> descricao;

	public static final String SITUACAO = "situacao";
	public static final String DATA_PAGAMENTO = "dataPagamento";
	public static final String DATA_VENCIMENTO = "dataVencimento";
	public static final String VALOR = "valor";
	public static final String ID = "id";
	public static final String DESCRICAO = "descricao";

}

