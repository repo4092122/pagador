package com.renango.pagador.service.mapper;

import com.renango.pagador.domain.Conta;
import com.renango.pagador.service.dto.ContaDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-30T11:51:33-0300",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 17.0.9 (GraalVM Community)"
)
@Component
public class ContaMapperImpl implements ContaMapper {

    @Override
    public Conta toEntity(ContaDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Conta conta = new Conta();

        conta.setId( dto.getId() );
        conta.setDataVencimento( dto.getDataVencimento() );
        conta.setDataPagamento( dto.getDataPagamento() );
        conta.setDescricao( dto.getDescricao() );
        conta.setSituacao( dto.getSituacao() );
        conta.setValor( dto.getValor() );

        return conta;
    }

    @Override
    public ContaDTO toDto(Conta entity) {
        if ( entity == null ) {
            return null;
        }

        ContaDTO contaDTO = new ContaDTO();

        contaDTO.setId( entity.getId() );
        contaDTO.setDataVencimento( entity.getDataVencimento() );
        contaDTO.setDataPagamento( entity.getDataPagamento() );
        contaDTO.setDescricao( entity.getDescricao() );
        contaDTO.setSituacao( entity.getSituacao() );
        contaDTO.setValor( entity.getValor() );

        return contaDTO;
    }

    @Override
    public List<Conta> toEntity(List<ContaDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Conta> list = new ArrayList<Conta>( dtoList.size() );
        for ( ContaDTO contaDTO : dtoList ) {
            list.add( toEntity( contaDTO ) );
        }

        return list;
    }

    @Override
    public List<ContaDTO> toDto(List<Conta> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ContaDTO> list = new ArrayList<ContaDTO>( entityList.size() );
        for ( Conta conta : entityList ) {
            list.add( toDto( conta ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Conta entity, ContaDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getDataVencimento() != null ) {
            entity.setDataVencimento( dto.getDataVencimento() );
        }
        if ( dto.getDataPagamento() != null ) {
            entity.setDataPagamento( dto.getDataPagamento() );
        }
        if ( dto.getDescricao() != null ) {
            entity.setDescricao( dto.getDescricao() );
        }
        if ( dto.getSituacao() != null ) {
            entity.setSituacao( dto.getSituacao() );
        }
        if ( dto.getValor() != null ) {
            entity.setValor( dto.getValor() );
        }
    }
}
